	myFeedApp.controller('feedController',function ($scope){
	
	$scope.getFeeds = function () {
			$scope.feeds = [];
			$scope.feedItems = localStorage.getItem('feeds');
			if($scope.feedItems!== null) {
				$scope.feeds = JSON.parse($scope.feedItems);
			}	
			return $scope.feeds;
	};
		
	$scope.feeds = $scope.getFeeds();

	$scope.setFile = function(element) {
		$scope.$apply(function($scope) {
			$scope.theFile = element.files[0];
        	});
	};
	
	$scope.doPost = function (obj) {
		if($scope.obj == undefined) {
			$scope.obj = '';
		}
		$scope.feeds.push({
			message:$scope.message,
			upVoteCount:0,
			image:obj.data,
			cmt : []
		});
			
		$scope.message = "";
		$scope.theFile = "";
		localStorage.setItem('feeds',JSON.stringify($scope.feeds));
	};
		
	$scope.doComment = function (feed) {
		//debugger;			
		feed.cmt.push(feed.comment);
		localStorage.setItem('feeds',JSON.stringify($scope.feeds));
		feed.comment = "";
	};
			
	$scope.upVote = function (feed) {
		feed.upVoteCount++;
		localStorage.setItem('feeds',JSON.stringify($scope.feeds));
	};	
	
});